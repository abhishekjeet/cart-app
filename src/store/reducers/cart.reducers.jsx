import * as types from '../types';

const initialState = { checkoutItems: [], totalItems: 0, totalPrice: 0 };

const cartReducer = (state = initialState, { type, payload }) => {
    debugger;
    switch (type) {
        case types.ADD_ITEM:
            let existingItem = state.checkoutItems.find(x => payload.id === x.id);
            if (existingItem) {
                let data = state.checkoutItems;
                
                data.map(item => {
                    if (item.id === payload.id) {
                        return item.quantity += 1;
                    }
                });
                return {
                    ...state,
                    checkoutItems: data,
                    totalItems: state.totalItems + 1,
                    totalPrice: state.totalPrice + payload.price
                }
            }
            else {
                payload.quantity = 1;
                return {
                    ...state,
                    checkoutItems: [...state.checkoutItems, payload],
                    totalItems: state.totalItems + 1,
                    totalPrice: state.totalPrice + payload.price

                }

            }
        case types.REMOVE_ITEM:
            let addedItem = state.checkoutItems.find(x => payload.id === x.id);
            if (addedItem.quantity === 1) {
                let updateItems = state.checkoutItems.filter(x => x.id !== payload.id)
                payload.quantity = 0;
                return {
                    ...state,
                    checkoutItems: updateItems,
                    totalItems: state.totalItems - 1,
                    totalPrice: state.totalPrice - addedItem.price
                }
            }
            else {
                let data = state.checkoutItems;
                data.map(item => {
                    if (item.id === payload.id) {
                        return item.quantity -= 1;
                    }
                })
                return {
                    ...state,
                    checkoutItems: data,
                    totalItems: state.totalItems - 1,
                    totalPrice: state.totalPrice - addedItem.price
                }

            }
        default:
            return state;
    }
}
export default cartReducer;