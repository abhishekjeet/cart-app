import * as types from "../types";


export const addItem = (data) => {
    return { type: types.ADD_ITEM, payload: data };
}

export const removeItem = (data) => {
    return { type: types.REMOVE_ITEM, payload: data };
}
