import React, { Fragment } from 'react'
import MaterialTable from 'material-table';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';

function CheckoutPage(props) {
    let data = props.items;
    return (
        <Fragment>
            <div className="tableContainer">
                <h3>Checkout Page</h3>
                <Button variant="contained" color="secondary" onClick={() => props.history.push("/")} >Back to Item List</Button>
                {(() => {
                    if (data.totalItems > 0) {
                        return <Fragment>
                            <div className="checkoutInfo">
                                <span><b>Total Items :</b></span>
                                <span>{data.totalItems}</span>
                            </div>
                            <div className="checkoutInfo">
                                <span><b>Total Checkout Price :</b></span>
                                <span>Rs.{data.totalPrice}</span>
                            </div>
                            <div className="tableContainer">
                                <MaterialTable
                                    title="Checkout Items List"
                                    columns={[
                                        { title: "Product", field: "name" },
                                        { title: "Price", field: "price" },
                                        { title: "Quantity", field: "quantity" },
                                        {
                                            title: "Total Amount",
                                            render: (rowData) => (
                                                rowData.price * rowData.quantity
                                            ),
                                        },
                                    ]}
                                    data={data.checkoutItems}
                                    options={{
                                        draggable: false,
                                        sorting: false,
                                        pageSize: 5,
                                        pageSizeOptions: [5, 10, 25, 50, 100],
                                        paging: true,
                                        headerStyle: {
                                            backgroundColor: "#4c5a67",
                                            color: "#FFF",
                                        }
                                    }}
                                    localization={{
                                        pagination: {
                                            labelRowsSelect: "rows per page",
                                        },
                                    }}
                                />
                            </div>
                        </Fragment>
                    }
                    else {
                        return <div className="checkoutMessage">
                            <h4>
                                Cart is empty. Go to list page to add items.
                                    </h4>
                        </div>
                    }
                })()}


            </div>
        </Fragment>
    )
}
const mapStateToProps = (state) => {

    return {
        items: state.cart
    }
}
export default connect(mapStateToProps)(CheckoutPage);