import React, { useState, useEffect, Fragment } from 'react'
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as actions from "../store/actions";
import image1 from "../assets/images/sessionimage3.jpg";
import image2 from "../assets/images/sessionimage4.jpg";
import image3 from "../assets/images/sessionimage5.jpg";
import image4 from "../assets/images/sessionimage6.jpg";
import image5 from "../assets/images/sessionimage7.jpg";

const initialData = [
    {
        id: 1,
        name: "Vivo Y19",
        image: image1,
        price: 15000,
        quantity: 0
    },
    {
        id: 2,
        name: "Vivo S1 PRO",
        image: image2,
        price: 8990,
        quantity: 0
    },
    {
        id: 3,
        name: "Vivo V7+",
        image: image3,
        price: 20500,
        quantity: 0
    },
    {
        id: 4,
        name: "Vivo V11 Pro",
        image: image4,
        price: 12500,
        quantity: 0
    },
    {
        id: 5,
        name: "Apple Iphone 11",
        image: image5,
        price: 72500,
        quantity: 0
    },
    {
        id: 6,
        name: "Apple Iphone 11 Pro",
        image: image2,
        price: 99000,
        quantity: 0
    },
    {
        id: 7,
        name: "Oppo Reno 2 F",
        image: image4,
        price: 23000,
        quantity: 0
    },
    {
        id: 8,
        name: "Oppo A71",
        image: image1,
        price: 9000,
        quantity: 0
    },
    {
        id: 9,
        name: "Oppo F15",
        image: image5,
        price: 21500,
        quantity: 0
    },
    {
        id: 10,
        name: "Samsung Galaxy A80",
        image: image1,
        price: 52000,
        quantity: 0
    }
];
const ListPage = (props) => {

    const [cartData, setCartData] = useState(initialData);
    function handleAddItem(item) {
        props.actions.addItem(item);
    }

    function handleRemoveItem(item) {
        props.actions.removeItem(item);
    }

    useEffect(() => {
        const tempCartData = cartData;
        const cartItem = props.items;
        if (Object.keys(cartItem.checkoutItems).length > 0) {
            debugger;
            tempCartData.map(item => {
                debugger;
                let existedItem = cartItem.checkoutItems.find(x => x.id === item.id);
                if (typeof (existedItem) !== "undefined") {
                    item.quantity = existedItem.quantity;
                }
            })
            setCartData(tempCartData);
        }
    }, []);
    return (
        <div className="col-md-12" >
            <h2>Items List</h2>
            <div className="checkoutButton">
                <Button variant="contained" onClick={() => props.history.push("/checkoutPage")} >Go To Checkout Page</Button>
            </div>
            <div className="gridSection">

                {cartData.map(item => (
                    <div className="row" >
                        <div className="col-md-4">
                            <img className="cartItemThumb" src={item.image}></img>
                        </div>
                        <div className="col-md-8">
                            <div className="discriptionCol">
                                <Fragment>
                                    <div className="date">
                                        <span className="mainCaption">
                                            {item.name}
                                        </span>
                                        <div className="divider"></div>
                                        <span className="itemPrice">{item.price}</span>
                                    </div>
                                </Fragment>
                            </div>
                            <div className="actionBtn">
                                <div className="row">
                                    <Button variant="outlined" onClick={() => handleAddItem(item)} > + </Button>
                                    <span> {item.quantity > 0 ? item.quantity : ''}</span>
                                    <Button variant="outlined" color="primary" disabled={item.quantity > 0 ? false : true} onClick={() => handleRemoveItem(item)} > - </Button>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
                }
            </div >
        </div >
    )

}

const mapStateToProps = (state) => {
    debugger;
    return {
        items: state.cart
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            addItem: bindActionCreators(actions.addItem, dispatch),
            removeItem: bindActionCreators(actions.removeItem, dispatch)
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListPage);