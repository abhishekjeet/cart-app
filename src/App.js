import React from 'react';
import AppLayout from "../src/shared/layout/AppLayout";
import './App.css';

function App() {
  return (
    <div className="App">
      <AppLayout />
    </div>
  )
}

export default App;
