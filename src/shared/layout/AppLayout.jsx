import React from 'react';
import ItemList from "../../containers/itemList";
import CheckoutPage from "../../containers/checkoutPage";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import { history } from "../../helpers/history";

export default function AppLayout() {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={ItemList} />
                <Route exact path="/checkoutPage" component={CheckoutPage} />
            </Switch>
        </Router>
    );
}
